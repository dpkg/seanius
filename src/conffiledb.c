/*
 * @file conffiledb.c
 *
 * Implementation of conffile database handling routines.
 *
 * Copyright © 2009 Sean Finney <seanius@debian.org>
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <config.h>
#include <compat.h>

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/wait.h>

#include <dpkg/dpkg.h>
#include <dpkg/i18n.h>
#include <dpkg/buffer.h>
#include <dpkg/path.h>
#include <dpkg/file.h>
#include <dpkg/dpkg-db.h>
#include <dpkg/varbuf.h>
#include <dpkg/subproc.h>

#include "main.h"
#include "conffiledb.h"

#define CONFFILEDBDIRROOT "conffiles" /**< @ref conffiledb root location */
/**
 * The subdirectory for storing merge parent versions of files.
 *
 * Note that this is required to be invalid with respect to policy-compliant
 * version strings, in order to avoid a namespace clash with versions in the
 * same directory for any given package.
 */
#define MERGEPARENTDIR "merge:parent"

/**
 * Return the canonical path for a conffiledb entry or other location.
 *
 * This is a helper function to resolve the full path to a specific location
 * within the conffile database. The primary use is to find the location
 * of a conffiledb file, but it can also be used to find the location
 * of the @ref conffiledb for a particular package as well as the parent
 * directory containing all conffiledb directories for a package.
 *
 * @param pkg The package information. If NULL, the returned value will
 *        be the conffiledb root location (admindir + #CONFFILEDBDIRROOT).
 * @param version Package version information. This is typically one of
 *        pkg.installed, pkg.available, or pkg.configversion. If NULL,
 *        the returned value will be the top level directory for pkg (i.e.
 *        the directory containing all version subdirectories).
 * @param path The absolute path to the conffile. If NULL, the returned
 *        value will be the conffiledb directory which should contain all
 *        conffiledb files for the combination of pkg and version.
 *
 * @return An allocated string with the path (your job to free it).
 */
char*
conffiledb_path(const struct pkginfo *pkg,
                const struct versionrevision *version, const char *path)
{
	struct varbuf full_path = VARBUF_INIT;
	char hash[MD5HASHLEN + 1];

	varbufprintf(&full_path, "%s/%s", admindir, CONFFILEDBDIRROOT);

	if (pkg != NULL) {
		varbufaddc(&full_path, '/');
		varbufaddstr(&full_path, pkg->name);
	}

	if (version != NULL) {
		varbufaddc(&full_path, '/');
		varbufversion(&full_path, version, vdew_nonambig);
	}

	/* if a conffile is being requested (not just the db root)... */
	if (path != NULL) {
		varbufaddc(&full_path, '/');
		buffer_md5(path, hash, strlen(path));
		varbufaddstr(&full_path, hash);
	}
	varbufaddc(&full_path, 0);

	debug(dbg_conffdetail, "confffiledb_path(%s, %s) = %s\n",
	      pkg?(pkg->name):"(null)", path, full_path.buf);

	return full_path.buf;
}

/** Ensure that a particular conffiledb dir exists.
 *
 * @param pkg The package owning the conffiledb dir.
 * @param version Which package version to reference. If NULL, the containing
 *        directory and any other necessary supporting files/dirs are created
 *        instead (i.e. the merge parent directory).
 */
static void
conffiledb_ensure_db(const struct pkginfo *pkg,
                     const struct versionrevision *version)
{
	struct stat s;
	struct varbuf merge_parent_fname = VARBUF_INIT;
	int i = 0;
	char *dbdirs[2];

	dbdirs[0] = conffiledb_path(pkg, NULL, NULL);
	if (version) {
		dbdirs[1] = conffiledb_path(pkg, version, NULL);
	} else {
		varbufprintf(&merge_parent_fname, "%s/%s", dbdirs[0],
		             MERGEPARENTDIR);
		dbdirs[1] = merge_parent_fname.buf;
	}

	/* now ensure each directory exists while reconstructing the path */
	for (i = 0; i < 2; i++) {
		if (stat(dbdirs[i], &s)) {
			debug(dbg_conffdetail,
			      "conffiledb_ensure_db: creating %s\n", dbdirs[i]);
			if (errno != ENOENT)
				ohshite("conffiledb_ensure_db: stat(%s)",
				        dbdirs[i]);
			if (mkdir(dbdirs[i], S_IRWXU))
				ohshite("conffiledb_ensure_db: mkdir(%s)",
				        dbdirs[i]);
		}
		free(dbdirs[i]);
	}
}

/**
 * Register a new conffiledb file for a package.
 *
 * Create a conffiledb entry in the conffiledb dir for the newly available
 * version of a particular package.
 *
 * The use of a filedescriptor is due to the fact that the new conffile is
 * intercepted very early during the unpack phase, before it exists on disk.
 * Therefore this interface is designed to hook cleanly into what's available
 * at that point, which is a filedescriptor generated from a struct TarInfo
 * (see tarobject in archives.c).
 *
 * @param pkg The package owning the conffile.
 * @param path The installed location of the conffile.
 * @param fd A file descriptor holding the contents of the conffile.
 * @param sz The size of the conffile.
 */
void
conffiledb_register_new_conffile(const struct pkginfo *pkg, const char *path, int fd,
                                 off_t sz)
{
	int cfgfd;
	/* get the path to where the registered file goes */
	char *p = conffiledb_path(pkg, &pkg->available.version, path);
	char fnamebuf[256];
	struct varbuf tmpfn = VARBUF_INIT;

	varbufprintf(&tmpfn, "%s%s", p, DPKGTEMPEXT);

	conffiledb_ensure_db(pkg, &pkg->available.version);
	debug(dbg_conff, "conffile_reg_fd: %s, %s, %s\n", pkg->name, path, p);
	/* make a mode 600 copy of file to the .dpkg-tmp location and then
	 * rename it to the proper destination */
	cfgfd = open(tmpfn.buf, (O_CREAT|O_EXCL|O_WRONLY), (S_IRUSR|S_IWUSR));
	if (cfgfd < 0)
		ohshite(_("unable to create `%.255s'"),tmpfn.buf);
	fd_fd_copy(fd, cfgfd, sz, _("backend dpkg-deb during `%.255s'"),
	           path_quote_filename(fnamebuf, tmpfn.buf, 256));
	fsync(cfgfd);
	if (close(cfgfd))
		ohshite("can't close %s", p);
	if (rename(tmpfn.buf, p))
		ohshite("can't rename registered conffile to '%s'", p);

	free(p);
	varbuffree(&tmpfn);
}

/**
 * Get a readable filehandle to the contents of a conffiledb entry for
 * a given package/version.
 *
 * @param pkg The package owning the conffile.
 * @param version The relevant package version.
 * @param path The installed location of the conffile.
 * @return A read-only file descriptor for the registered conffile.
 */
int
conffiledb_get_conffile(const struct pkginfo *pkg,
                        const struct versionrevision *version, const char *path)
{
	int fd;
	char *p = conffiledb_path(pkg, version, path);

	fd = open(p, O_RDONLY);
	if (fd == -1)
		ohshite("error opening conffile registered at %s", p);

	free(p);
	return fd;
}

/**
 * Remove a conffiledb directory for a package.
 *
 * @param pkg The package owning the conffile db to be removed.
 * @param version The version of the package for which to remove the conffiledb.
 *        If set to NULL, all information for pkg will be removed.
 */
void
conffiledb_remove(const struct pkginfo *pkg,
                  const struct versionrevision *version)
{
	char *p = conffiledb_path(pkg, version, NULL);

	debug(dbg_conff, "conffiledb_remove(%s): removing %s\n", pkg->name, p);
	ensure_pathname_nonexisting(p);

	free(p);
}

/**
 * Record the common ancestor of a successful merge.
 *
 * Assuming pristine v1 conffile was edited locally by the user, and
 * then successfully merged with pristine v2 conffile, we then want to
 * record v1 as a "common ancestor" of the local file and v2, which
 * may be helpful in future merges.
 *
 * @param pkg the package owning the conffile
 * @param version the package version of the "common ancestor"
 * @param path the path to the conffile
 */
void
conffiledb_set_merge_parent(const struct pkginfo *pkg,
                            const struct versionrevision *version,
                            const char *path)
{
	char *src_fname, *pkg_dir, hash[MD5HASHLEN + 1];
	struct varbuf dst_fname_buf = VARBUF_INIT, tmp_fname_buf = VARBUF_INIT;
	int src_fd, tmp_fd;

	conffiledb_ensure_db(pkg, NULL);

	pkg_dir = conffiledb_path(pkg, NULL, NULL);
	src_fname = conffiledb_path(pkg, version, path);
	buffer_md5(path, hash, strlen(path));
	varbufprintf(&dst_fname_buf, "%s/%s/%s", pkg_dir, MERGEPARENTDIR, hash);
	varbufprintf(&tmp_fname_buf, "%s%s", dst_fname_buf.buf, DPKGTEMPEXT);

	src_fd = open(src_fname, O_RDONLY);
	if (src_fd == -1)
		ohshite("conffiledb_register_merge_parent: src open failed");

	tmp_fd = open(tmp_fname_buf.buf, O_WRONLY|O_CREAT|O_TRUNC, 0600);
	if (tmp_fd == -1)
		ohshite("conffiledb_register_merge_parent: tmp open failed");

	fd_fd_copy(src_fd, tmp_fd, -1, "conffiledb_register_merge_parent");

	if (close(src_fd) == -1 || close(tmp_fd) == -1)
		ohshite("conffiledb_register_merge_parent: close failed");

	if (rename(tmp_fname_buf.buf, dst_fname_buf.buf) == -1)
		ohshite("conffiledb_register_merge_parent: rename failed");

	free(pkg_dir);
	free(src_fname);
	varbuffree(&dst_fname_buf);
	varbuffree(&tmp_fname_buf);
}

/**
 * Remove the common ancestor of a previous merge.
 *
 * Assuming the user later opts to install the pristine version of
 * a conffile, we should forget the common ancestor.
 *
 * @param pkg the package owning the conffile
 */
void
conffiledb_remove_merge_parent(const struct pkginfo *pkg, const char *path)
{
	char *pkg_dir, hash[MD5HASHLEN + 1];
	struct varbuf dst_fname_buf = VARBUF_INIT;

	pkg_dir = conffiledb_path(pkg, NULL, NULL);
	buffer_md5(path, hash, strlen(path));
	varbufprintf(&dst_fname_buf, "%s/%s/%s", pkg_dir, MERGEPARENTDIR, hash);

	ensure_pathname_nonexisting(dst_fname_buf.buf);

	free(pkg_dir);
	varbuffree(&dst_fname_buf);
}

/**
 * Attempt to automagically merge a 3-way delta for a conffile.
 *
 * On a successful merge, the "common ancestor" is recorded for the package
 * by placing a copy in the package's MERGEPARENTDIR subdirectory.
 *
 * @param pkg The package owning the conffile
 * @param path The path to the installed conffile
 * @return The exit status of the underlying call to diff3(1)
 */
int conffiledb_automerge(const struct pkginfo *pkg, const char *path)
{
	int res, merge_fd, merge_pipe[2];
	pid_t child_pid;
	struct varbuf merge_output_fname = VARBUF_INIT;
	char *cf_old = NULL, *cf_new = NULL;

	cf_old = conffiledb_path(pkg, &pkg->installed.version, path);
	cf_new = conffiledb_path(pkg, &pkg->available.version, path);

	/* create a file for merge output, ensuring it does not already exist */
	varbufprintf(&merge_output_fname, "%s%s", path, DPKGMERGEEXT);
	merge_fd = open(merge_output_fname.buf, O_CREAT|O_WRONLY|O_EXCL);
	if (merge_fd == -1)
		ohshite("conffiledb_automerge: can't open %s for merge output",
		        merge_output_fname.buf);

	/* fork a child for diff3 and catch its stdout in a pipe */
	if (pipe(merge_pipe) == -1)
		ohshite("conffiledb_automerge: pipe failed");
	child_pid = m_fork();
	if (!child_pid) {
		if(dup2(merge_pipe[1], STDOUT_FILENO) == -1)
			ohshite("conffiledb_automerge: child dup2 failed");
		if(close(merge_pipe[0]) == -1 || close(merge_pipe[1]) == -1)
			ohshite("conffiledb_automerge: child close failed");
		execlp(DIFF3, "diff3", "-m", cf_new, cf_old, path, NULL);
		ohshite(_("failed to exec DIFF3 for merge"));
	}
	debug(dbg_conffdetail, "conffiledb_automerge: merging");

	/* now copy the piped output into the merge results file */
	if (close(merge_pipe[1]) == -1)
		ohshite("conffiledb_automerge: write pipe close failed");
	file_copy_perms(path, merge_output_fname.buf);
	fd_fd_copy(merge_pipe[0], merge_fd, -1, "conffiledb_automerge");
	if (close(merge_fd) == -1 || close(merge_pipe[0]) == -1)
		ohshite("conffiledb_automerge: merge/read pipe close failed");

	res = subproc_wait(child_pid, DIFF3);
	if (WEXITSTATUS(res) == 0) {
		/* rename the merge output to the final destination */
		if (rename(merge_output_fname.buf, path) == -1)
			ohshite("conffiledb_automerge: can not rename %s",
			        merge_output_fname.buf);
	} else {
		/* oh noes, merge failed :( */
		if (unlink(merge_output_fname.buf))
			ohshite("conff_automerge: can not unlink %s",
			        merge_output_fname.buf);
	}

	varbuffree(&merge_output_fname);

	free(cf_new);
	free(cf_old);

	return res;
}
