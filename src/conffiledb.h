/**
 * @file conffiledb.h
 *
 * Functions for tracking referencable versions of packages' conffiles.
 *
 * This library allows for tracking the pristine versions (and in some specific
 * cases a few other versions) of packages' conffiles.  This allows for a
 * number of desirable features, such as conffile merging and more
 * featureful reporting of the differences between conffiles on package
 * upgrade even when the local admin may have modified or removed the installed
 * version of the file.
 *
 * The internal database of conffiles follows the convention:
 *
 *	#CONFFILEDBDIRROOT/\<pkg\>/\<version\>/\<md5\>[.\<ext\>]
 *
 * where:
 * - @b \<pkg\> is the conffile database directory for an individual package.
 * - @b \<version\> is the relevant version of the package (i.e. in most
 *   cases the version of the package shipping the conffile).
 * - @b \<md5\> is the md5 hash of the location of a package's conffile. A
 *   hash is used to keep a flat and balanced directory structure, and has
 *   the added benefit of a simpler implementation.
 * - @b \<ext\> is a short string used to identify other versions of the
 *   conffile, such as merged versions.
 *
 * Some further terminology to keep things clear later on:
 *
 * - @b "conffiledb root" refers to the top-most directory containing
 *   all conffile databases, and is defined by #CONFFILEDBDIRROOT.
 * - @anchor conffiledb @b "conffile database" or @b "conffiledb" refers
 *   to a directory specific to a package + version tuple.  Such a
 *   directory will contain copies of the conffiles named after the hash
 *   of their respective installed locations.
 * - @b "conffiledb file" or @b "conffiledb entry" refers to a file whose
 *   contents correspond to a particular conffiledb_base/package/conffile
 *   triplet.
 *
 * Copyright © 2009 Sean Finney <seanius@debian.org>
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DPKG_CONFFILEDB_H
#define DPKG_CONFFILEDB_H

#include <sys/types.h>

char* conffiledb_path(const struct pkginfo *pkg,
                      const struct versionrevision *version, const char *path);

void conffiledb_register_new_conffile(const struct pkginfo *pkg,
                                      const char *path, int fd, off_t sz);

int conffiledb_get_conffile(const struct pkginfo *pkg,
                            const struct versionrevision *version,
                            const char *path);

void conffiledb_remove(const struct pkginfo *pkg,
                       const struct versionrevision *version);

int conffiledb_automerge(const struct pkginfo *pkg, const char *path);

void conffiledb_set_merge_parent(const struct pkginfo *pkg,
                                 const struct versionrevision *version,
                                 const char *path);

void conffiledb_remove_merge_parent(const struct pkginfo *pkg,
                                    const char *path);

#endif /* DPKG_CONFFILEDB_H */
