/**
 * @file util.c Miscellaneous utility functions
 *
 * Copyright © 1995 Ian Jackson <ian@chiark.greenend.org.uk>
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2,
 * or (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with dpkg; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <config.h>
#include <compat.h>

#include <assert.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include "main.h"

#include "dpkg/dpkg.h"
#include "dpkg/i18n.h"
#include "dpkg/path.h"
#include "dpkg/subproc.h"

/**
 * General purpose debug output function
 *
 * @param which The logging category/severity, which should correspond to
 *        one or more bitwise maskings of enum debugflags. This parameter
 *        is then masked against the externally linked f_debug flag from
 *        the main program (which in turn is set by -Dh in the dpkg(1) main
 *        program, for example). In the case of a match the given format
 *        string and arguments are printed.
 * @param fmt A standard printf(1) compatible format string, followed by any
 *        formatting variables.
 */
void
debug(int which, const char *fmt, ...)
{
	va_list ap;
	if (!(f_debug & which)) return;
	fprintf(stderr,"D0%05o: ",which);
	va_start(ap,fmt);
	vfprintf(stderr,fmt,ap);
	va_end(ap);
	putc('\n',stderr);
}

/**
 * Delete a pathname and anything that may be beneath it.
 *
 * This function is a generic catch-all delete utility, which
 * will remove anything that may exist at the given pathname.  If
 * The pathname is a nonempty directory, it will be recursively
 * removed with a fork/exec of rm -rf.
 *
 * @param pathname the path to be removed.
 */
void
ensure_pathname_nonexisting(const char *pathname)
{
	int c1;
	const char *u;

	u = path_skip_slash_dotslash(pathname);
	assert(*u);

	debug(dbg_eachfile,"ensure_pathname_nonexisting `%s'",pathname);
	if (!rmdir(pathname)) return; /* Deleted it OK, it was a directory. */
	if (errno == ENOENT || errno == ELOOP) return;
	if (errno == ENOTDIR) {
		/* Either it's a file, or one of the path components is.  If one
		 * of the path components is this will fail again ...
		 */
		if (secure_unlink(pathname) == 0)
			return; /* OK, it was */
		if (errno == ENOTDIR) return;
	}
	if (errno != ENOTEMPTY && errno != EEXIST) { /* Huh ? */
		ohshite(_("unable to securely remove '%.255s'"), pathname);
	}
	c1= m_fork();
	if (!c1) {
		execlp(RM, "rm", "-rf", "--", pathname, NULL);
		ohshite(_("failed to exec rm for cleanup"));
	}
	debug(dbg_eachfile,"ensure_pathname_nonexisting running rm -rf");
	subproc_wait_check(c1,"rm cleanup",0);
}

/**
 * Safely unlink a given pathname, with checks on file type and mode.
 *
 * If the pathname to remove is:
 *
 * -# a sticky or set-id file, or
 * -# an unknown object (i.e., not a file, link, directory, fifo or socket)
 *
 * we change its mode so that a malicious user cannot use it, even if it's
 * linked to another file.
 *
 * @param pathname the path to unlink
 * @return 0 on success, -1 on error.
 */
int
secure_unlink(const char *pathname)
{
	struct stat stab;

	if (lstat(pathname,&stab))
		return -1;

	return secure_unlink_statted(pathname, &stab);
}

/**
 * Safely unlink a given pathname, checking file type and mode against
 * externally provided file type and mode information.
 *
 * See notes in secure_unlink.
 *
 * @param pathname the path to unlink.
 * @param stab a struct stat containing information about the path.
 * @return 0 on success, -1 on error.
 */
int
secure_unlink_statted(const char *pathname, const struct stat *stab)
{
	if (S_ISREG(stab->st_mode) ? (stab->st_mode & 07000) :
			!(S_ISLNK(stab->st_mode) || S_ISDIR(stab->st_mode) ||
			  S_ISFIFO(stab->st_mode) || S_ISSOCK(stab->st_mode))) {
		if (chmod(pathname, 0600))
			return -1;
	}
	if (unlink(pathname))
		return -1;
	return 0;
}
