/**
 * @file t-conffiledb.c
 *
 * Tests for various aspects of the conffile database.
 *
 * Copyright © 2009 Sean Finney <seanius@debian.org>
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <dpkg/test.h>
#include <dpkg/dpkg-db.h>
#include <dpkg/varbuf.h>
#include <dpkg/buffer.h>
#include <../src/conffiledb.h>

#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/fcntl.h>
#include <unistd.h>

/* Forward declaration to avoid needing to include main.h */
void ensure_pathname_nonexisting(const char *pathname);

/**
 * Our "main program" admindir, which we set to a temporary
 * location for our tests.
 */
char *admindir;

/* Convenience pointers used for inspection of conffiledb contents */
char *conffiledbdir, *mergeparentdir;

/**
 * Flags for controlling output settings in debug().
 *
 * This is needed to link properly with debug() but also convenient
 * for debugging. Set to 0 to keep things sane and something like
 * (dbg_conff|dbg_conffdetail) to get all the conffiledb debug output
 * during test execution.
 */
int f_debug = 0;

/* some global fixtures used in both tests */
static struct pkginfo pkg_a, pkg_a_v2;
static char *pkg_a_cnff_1;
static char *pkg_a_cnff_1_md5;

/**
 * Initialization of various test fixtures etc
 */
static void
setup(void)
{
	admindir = m_strdup("/tmp/dpkgtest-admindir-XXXXXX");
	if (mkdtemp(admindir) == NULL)
		ohshite("mkdtemp failed in setup");

	conffiledbdir = m_malloc(strlen(admindir) + strlen("/conffiles") + 1);
	strcpy(conffiledbdir, admindir);
	strcat(conffiledbdir, "/conffiles");
	if (mkdir(conffiledbdir, 0777))
		ohshite("mkdir failed in setup");

	mergeparentdir = m_malloc(strlen(conffiledbdir) + 1 +
	                          strlen("/foopkg/merge:parent") + 1);
	sprintf(mergeparentdir, "%s/%s", conffiledbdir, "/foopkg/merge:parent");

	pkg_a.name = m_strdup("foopkg");
	pkg_a.installed.version.epoch = 1;
	pkg_a.installed.version.version = m_strdup("2.3");
	pkg_a.installed.version.revision = m_strdup("4deb5");
	pkg_a.available.version.epoch = 1;
	pkg_a.available.version.version = m_strdup("2.3");
	pkg_a.available.version.revision = m_strdup("5deb6");

	pkg_a_v2.name = m_strdup("foopkg");
	pkg_a_v2.installed.version.epoch = 1;
	pkg_a_v2.installed.version.version = m_strdup("2.3");
	pkg_a_v2.installed.version.revision = m_strdup("5deb6");
	pkg_a_v2.available.version.epoch = 1;
	pkg_a_v2.available.version.version = m_strdup("2.3");
	pkg_a_v2.available.version.revision = m_strdup("6deb7");

	/* in real use this would be under /etc, but here we set it to
	 * be under the admindir to allow for testing various things */
	pkg_a_cnff_1 = m_malloc(strlen(admindir) + strlen("/foo.cfg") + 1);
	strcpy(pkg_a_cnff_1, admindir);
	strcat(pkg_a_cnff_1, "/foo.cfg");

	pkg_a_cnff_1_md5 = m_malloc(MD5HASHLEN + 1);
	buffer_md5(pkg_a_cnff_1, pkg_a_cnff_1_md5, strlen(pkg_a_cnff_1));
}

/**
 * Cleanup to be done when tests are done.
 *
 * Any files creating during tests should be removed here, as well
 * as the free()'ing of any allocated memory. This should allow for
 * testing that the various routines do not leave cruft behind or
 * leak any memory.
 */
static void
teardown(void)
{
	free((char*)pkg_a.name);
	free((char*)pkg_a.installed.version.version);
	free((char*)pkg_a.installed.version.revision);
	free((char*)pkg_a.available.version.version);
	free((char*)pkg_a.available.version.revision);

	free((char*)pkg_a_v2.name);
	free((char*)pkg_a_v2.installed.version.version);
	free((char*)pkg_a_v2.installed.version.revision);
	free((char*)pkg_a_v2.available.version.version);
	free((char*)pkg_a_v2.available.version.revision);

	free(pkg_a_cnff_1);
	free(pkg_a_cnff_1_md5);

	ensure_pathname_nonexisting(admindir);
	free(admindir);
	free(conffiledbdir);
	free(mergeparentdir);
}

/**
 * Ensure that conffiledb_path behaves sanely for all defined uses.
 */
static void
test_conffiledb_paths(void)
{
	char *path, *subdir;

	/* should be the conffiledb root dir */
	path = conffiledb_path(NULL, NULL, NULL);
	test_str(path, ==, conffiledbdir);
	free(path);

	/* should be root dir + package name */
	path = conffiledb_path(&pkg_a, NULL, NULL);
	subdir = path + strlen(admindir);
	test_str(subdir, ==, subdir);
	free(path);

	/* should be root dir + package name + version */
	path = conffiledb_path(&pkg_a, &pkg_a.installed.version, NULL);
	subdir = path + strlen(admindir);
	test_str(subdir, ==, "/conffiles/foopkg/1:2.3-4deb5");
	free(path);

	/* should be root dir + package name + version + hash */
	path = conffiledb_path(&pkg_a, &pkg_a.installed.version, pkg_a_cnff_1);
	subdir = path + strlen(admindir) +
	         strlen("/conffiles/foopkg/1:2.3-4deb5/");
	test_str(subdir, ==, pkg_a_cnff_1_md5);
	free(path);
}

static void
test_conffiledb_storage(void)
{
	int pipefds[2];
	struct stat stat_buf;
	int fetched_fd;
	const char *fake_content = "pretend conffile content\n";
	int content_sz = strlen(fake_content);
	char *expected_path = conffiledb_path(&pkg_a, &pkg_a.available.version,
	                                      pkg_a_cnff_1);

	/* setup a fake pipe like what we'd get in processarchive */
	if (pipe(pipefds))
		ohshite("pipe failed");
	if (write(pipefds[1], fake_content, content_sz) != content_sz)
		ohshite("write failed");
	if (close(pipefds[1]))
		ohshite("close failed");

	/* ensure the file registers, exists, and is the same size */
	conffiledb_register_new_conffile(&pkg_a, pkg_a_cnff_1, pipefds[0],
	                                 content_sz);
	test_pass(stat(expected_path, &stat_buf) == 0);
	test_pass(stat_buf.st_size == content_sz);

	/* now pretend it's later and test fetching the same file */
	fetched_fd = conffiledb_get_conffile(&pkg_a_v2,
	                                     &pkg_a_v2.installed.version,
	                                     pkg_a_cnff_1);
	test_pass(fstat(fetched_fd, &stat_buf) == 0);
	test_pass(stat_buf.st_size == content_sz);

	/* now pretend it's removal time */
	conffiledb_remove(&pkg_a_v2, &pkg_a_v2.installed.version);
	test_pass(stat(expected_path, &stat_buf) == -1);

	close(fetched_fd);
	free(expected_path);
}

/**
 * Test that the autmatic merging functionality works as intended.
 */
static void
test_conffiledb_merge(void)
{
	int install_fd, verify_fd, pipefds[2];
	char *content_old = m_strdup("one\ntwo\nthree\nfour\n");
	char *content_local = m_strdup("one\nfoo\nthree\nfour\n");
	char *content_new = m_strdup("one\ntwo\nthree\nfour\nfive\n");
	char *content_merged = m_strdup("one\nfoo\nthree\nfour\nfive\n");
	char *read_buf = m_malloc(64); /* just needs to be bigger than above */
	int fake_sz_old = strlen(content_old);
	int fake_sz_local = strlen(content_local);
	int fake_sz_new = strlen(content_new);
	size_t merge_result, read_sz;

	/* setup a fake pipe like before */
	if (pipe(pipefds))
		ohshite("old pipe failed");
	if (write(pipefds[1], content_old, fake_sz_old) != fake_sz_old)
		ohshite("old write failed");
	if (close(pipefds[1]))
		ohshite("old close failed");
	conffiledb_register_new_conffile(&pkg_a, pkg_a_cnff_1, pipefds[0],
	                                 fake_sz_old);

	/* install the "modified" version that the "admin" changed */
	install_fd = open(pkg_a_cnff_1, O_WRONLY|O_CREAT|O_TRUNC, 0700);
	if (install_fd < 0)
		ohshite("install open failed");
	if (write(install_fd, content_local, fake_sz_local) != fake_sz_local)
		ohshite("install write failed");

	/* here now pretend it's later and we're installing a new file */
	if (pipe(pipefds))
		ohshite("new pipe failed");
	if (write(pipefds[1], content_new, fake_sz_new) != fake_sz_new)
		ohshite("new write failed");
	if (close(pipefds[1]))
		ohshite("new close failed");
	conffiledb_register_new_conffile(&pkg_a_v2, pkg_a_cnff_1, pipefds[0],
	                                 fake_sz_new);

	merge_result = conffiledb_automerge(&pkg_a_v2, pkg_a_cnff_1);
	test_pass(merge_result == 0);

	/* now double check everything is as expected */
	verify_fd = open(pkg_a_cnff_1, O_RDONLY);
	if(verify_fd < 0)
		ohshite("verify open failed");
	read_sz = read(verify_fd, read_buf, 64);
	test_pass(read_sz == strlen(content_merged));
	read_buf[read_sz] = '\0';
	test_str(read_buf, ==, content_merged);

	if (close(verify_fd))
		ohshite("verify close failed");

	free(content_old);
	free(content_local);
	free(content_new);
	free(content_merged);
	free(read_buf);
}

/**
 * Test that the recording of merge parents works correctly.
 */
static void
test_conffiledb_record_mergeparent(void)
{
	int install_fd;
	char *content_local = m_strdup("one\nfoo\nthree\nfour\n");
	char *read_buf = m_malloc(64); /* just needs to be bigger than above */
	int fake_sz_local = strlen(content_local);
	size_t merge_result, read_sz;
	struct stat stat_buf;
	struct varbuf expected_parent_location = VARBUF_INIT;

	/* install an arbitrary conffile and enter it into the conffiledb */
	install_fd = open(pkg_a_cnff_1, O_RDWR|O_CREAT|O_TRUNC, 0700);
	if (install_fd < 0)
		ohshite("install open failed");
	if (write(install_fd, content_local, fake_sz_local) != fake_sz_local)
		ohshite("install write failed");
	if (lseek(install_fd, 0, SEEK_SET) == -1)
		ohshite("install seek failed");
	conffiledb_register_new_conffile(&pkg_a, pkg_a_cnff_1, install_fd,
	                                 fake_sz_local);

	/* record the merge and ensure it shows up correctly */
	varbufprintf(&expected_parent_location, "%s/%s", mergeparentdir,
	             pkg_a_cnff_1_md5);
	test_pass(stat(expected_parent_location.buf, &stat_buf) == -1);
	conffiledb_set_merge_parent(&pkg_a, &pkg_a.available.version,
	                            pkg_a_cnff_1);
	test_pass(stat(expected_parent_location.buf, &stat_buf) == 0);
	test_pass(stat_buf.st_size == fake_sz_local);

	/* double check that recording the removal also works */
	conffiledb_remove_merge_parent(&pkg_a, pkg_a_cnff_1);
	test_pass(stat(expected_parent_location.buf, &stat_buf) == -1);

	free(content_local);
	free(read_buf);
	varbuffree(&expected_parent_location);
}


static void
test(void)
{
	setup();

	test_conffiledb_paths();
	test_conffiledb_storage();
	test_conffiledb_merge();
	test_conffiledb_record_mergeparent();

	teardown();
}
